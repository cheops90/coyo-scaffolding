(function () {
  'use strict';
  var WorkspaceDetail = require('./../../workspaces/workspace-details.page.js');
  var App = require('./../app.page.js');
  var ListApp = require('./list.page.js');

  var workspaceDetail = new WorkspaceDetail();
  var listApp = new ListApp();
  var app = new App();
  var component = require('./../../../components.page');

  function ListUtil() {
    var api = this;

    /*
     Creates an empty List app
     */
    api.createEmptyListApp = function (noReadPermissions) {
      var navName = 'TestListAppNavigationName';
      var listName = 'ListAppName';

      // add new listapp
      workspaceDetail.options.addApp.click();
      app.app.list.click();

      // add name and settings
      listApp.setting.ListNameNavigation.clear();
      listApp.setting.ListNameNavigation.sendKeys(navName);

      expect(listApp.setting.active.isChecked()).toBeTruthy();

      listApp.setting.modalName.clear();
      listApp.setting.modalName.sendKeys(listName);

      listApp.setting.advanced.click();
      listApp.setting.createEntryGroup.yes.click();

      if (noReadPermissions) {
        listApp.setting.seeEntryGroup.none.click();
      }

      component.modals.confirm.confirmButton.click();
      browser.waitForAngular();
    };


    api.createUserField = function (userFieldName) {
      api.addField();
      listApp.fieldTypes.user.click();

      // add user field
      listApp.fieldSettingsUser.name.clear();
      listApp.fieldSettingsUser.name.sendKeys(userFieldName);

      listApp.fieldSettingsUser.required.click();
      component.modals.confirm.confirmButton.click();
    };

    api.createCheckboxField = function (checkboxFieldName) {
      api.addField();
      listApp.fieldTypes.checkbox.click();
      listApp.fieldSettingsCheckbox.name.clear();
      listApp.fieldSettingsCheckbox.name.sendKeys(checkboxFieldName);
      component.modals.confirm.confirmButton.click();
    };

    api.createOptionsField = function (optionFieldName, option1, option2, option3) {
      api.addField();
      listApp.fieldTypes.option.click();

      listApp.fieldSettingsUser.name.clear();
      listApp.fieldSettingsOption.name.sendKeys(optionFieldName);

      listApp.fieldSettingsOption.option(0).sendKeys(option1);
      listApp.fieldSettingsOption.addOptionBtn.click();
      listApp.fieldSettingsOption.option(1).sendKeys(option2);
      listApp.fieldSettingsOption.addOptionBtn.click();
      listApp.fieldSettingsOption.option(2).sendKeys(option3);
      component.modals.confirm.confirmButton.click();
    };

    api.createTextField = function (textFieldName) {
      api.addField();
      listApp.fieldTypes.text.click();
      listApp.fieldSettingsText.name.clear();
      listApp.fieldSettingsText.name.sendKeys(textFieldName);
      component.modals.confirm.confirmButton.click();
    };

    // choose if the entry is the first one or an entry already exists
    api.addField = function () {
      var newFieldBtn = listApp.listAppTable.newFieldBtn;
      newFieldBtn.isPresent().then(function (bool) {
        if (bool) {
          newFieldBtn.click();
        } else {
          listApp.configureFields.modalBtn.click();
          listApp.configureFields.configureFieldsButton.click();
          listApp.configureFields.addFieldBtn.click();
        }
      });
    };

  }

  module.exports = ListUtil;

})();
