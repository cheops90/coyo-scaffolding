(function () {
  'use strict';

  function PageImprint() {
    var api = this;

    api.title = element(by.css('.page-imprint .panel-title'));
    api.admins = {
      admins: element.all(by.repeater('admin in $ctrl.admins'))
    };

    api.widgetSlot = element(by.css('.page-imprint .widget-slot'));

    api.options = {
      edit: $('.imprint-edit-content .edit-header a[ng-click="$ctrl.edit()"]'),
      cancel: $('.imprint-edit-content .edit-header a[ng-click="$ctrl.cancel()"]'),
      save: $('.imprint-edit-content .edit-header a[ng-click="$ctrl.save()"]')
    };
  }

  module.exports = PageImprint;

})();
