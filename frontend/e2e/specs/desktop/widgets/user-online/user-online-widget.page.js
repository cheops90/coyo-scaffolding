(function () {
  'use strict';

  var extend = require('util')._extend;
  function UserOnlineWidget(widget) {
    var api = extend(this, widget);

    api.renderedWidget = {
      onlineUsersCount: api.container.$('.user-online-count')
    };
  }
  module.exports = UserOnlineWidget;
})();
