(function () {
  'use strict';

  var extend = require('util')._extend;
  function BookmarkingWidget(widget) {
    var api = extend(this, widget);

    api.addBookmark = {
      title: api.container.$('[ng-model="$ctrl.bookmarkTitle"]'),
      url: api.container.$('[ng-model="$ctrl.bookmarkUrl"]'),
      saveButton: api.container.$('[ng-click="addBookmarkForm.bookmarkUrl.$valid && $ctrl.addBookmark()"]')
    };

    api.editBookmark = {
      title: api.container.$('[ng-model="bookmark.title"]'),
      url: api.container.$('[ng-model="bookmark.url"]'),
      urlButton: api.container.$('[ng-click="$ctrl.editUrlWithIndex = $index"]'),

      bookmarkHasError: function () {
        return api.container.$('.zmdi-bookmark').getAttribute('class').then(function (classes) {
          return classes.split(' ').indexOf('has-error') !== -1;
        });
      }
    };

    api.bookmark = api.container.element(by.css('.bookmark-title a span'));
  }
  module.exports = BookmarkingWidget;
})();
